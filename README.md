# Machine learning
## Courses:
*  [Andrew Ng’s courses](https://www.coursera.org/learn/machine-learning) (An excellent starting point)
*  [Stéphane Mallat’s courses](https://www.college-de-france.fr/site/stephane-mallat/index.htm) (quite theoretical but fascinating)

## Blogs:
*  [AI experiments with google](https://experiments.withgoogle.com/collection/ai)
*  [sebastian raschka’s blog](https://sebastianraschka.com/blog/index.html)

## Oriented toward ML competitions:

*  [Mlwaves](https://mlwave.com/)
*  [FastML](http://fastml.com/)

# Deep-learning
## Vulgarisation:

*  [Réseaux de neurones](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi)
*  [Deep learning](https://www.youtube.com/watch?v=trWrEWfhTVg)

## Blog:
*  http://karpathy.github.io/2015/05/21/rnn-effectiveness/
*  http://colah.github.io/


## Courses:
http://course.fast.ai/index.html (completely free)
Andrew Ng’s courses on deep learning (apparently, it is possible to access to most of the course freely, you have to select “continuer le cours en tant qu’auditeur libre” when subscribing and not the free trial!)
